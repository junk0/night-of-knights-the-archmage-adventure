### What is this repository for? ###

* In our class, Game Development and Algorithmic Problem Solving 2, we are tasked with creating a game over the course of a semester. 

* This game was to be written in C#, making use of the [MonoGame](http://www.monogame.net/) framework.

* __This project was completed in May of 2017. This repository is a fork of the original, which was private during devlopment.__

### How do I play? ###

Grab the precompiled binary from the downloads section of this repository!

The controls are:

* WASD or arrow keys to move

* Left-click or 'E' to attack

* Right-click or 'Q' to block (you can block as long as the blue bar under your health is not empty!)

### Who created this? ###
* [Ben Fairlamb](mailto:btf6119@g.rit.edu) - Team Lead
* [TJ Wolschon](mailto:tjw3948@g.rit.edu) - Architect
* [Zack Dunham](mailto:zsd7200@g.rit.edu) - UI/Art direction
* [Michael Schek](mailto:mjs9513@g.rit.edu) - Game design